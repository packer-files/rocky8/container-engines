#!/bin/bash -eux

## Sarus
yum install -y wget
mkdir -p /opt/sarus/
wget -qO- https://github.com/eth-cscs/sarus/releases/download/1.4.2/sarus-Release.tar.gz \
    |tar xfz - -C /opt/sarus/ --strip-component=1
/opt/sarus/configure_installation.sh
ln -s /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem /etc/ssl/cert.pem
cat > /etc/profile.d/sarus.sh <<EOF
export PATH=/opt/sarus/bin:${PATH}
EOF

### Podman
yum install -y podman

### Docker CE
yum install -y yum-utils
yum config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo
yum config-manager --set-disabled docker-ce-stable
rpm --install --nodeps --replacefiles --excludepath=/usr/bin/runc \
    https://download.docker.com/linux/centos/8/x86_64/stable/Packages/containerd.io-1.4.9-3.1.el8.x86_64.rpm
yum install -y --enablerepo=docker-ce-stable docker-ce
systemctl enable --now  docker
usermod -aG docker alice
usermod -aG docker bob
usermod -aG docker vagrant


### Enroot
yum install -y git gcc make libcap libtool automake jq squashfs-tools parallel fuse-overlayfs pigz squashfuse
git clone --recurse-submodules https://github.com/NVIDIA/enroot.git /usr/local/src/enroot
cd /usr/local/src/enroot
export prefix=/usr
make install
make setcap
cd 
rm -rf /usr/local/src/enroot
